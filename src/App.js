import React from "react";
import Home from "./pages/Home";
import './index.css'

function App() {
  return (
    <div className="App">
      <Home />
    </div>
  );
}
document.addEventListener("contextmenu", (event) => event.preventDefault());

document.addEventListener("keydown", (event) => {
  if (
    (event.keyCode === 123,
    "I" || (event.ctrlKey && event.altKey && event.shiftKey))
  ) {
    event.preventDefault();
  }
});

export default App;
