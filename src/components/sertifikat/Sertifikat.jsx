import React, { Component } from "react";
import ras1 from '../../assets/images/sertifikat/ras1.png'
import ras2 from '../../assets/images/sertifikat/ras2.png'
import ras3 from '../../assets/images/sertifikat/ras3.png'
import ras4 from '../../assets/images/sertifikat/ras4.png'
import ras5 from '../../assets/images/sertifikat/ras5.png'
import Slider from "react-slick";
import './sertifikat.scss'

export default class SimpleSlider extends Component {
  render() {
    const settings = {

      dots: true,
      
      infinite: true,
      speed: 500,
      autoplay: true,
      slidesToShow: 5,
      slidesToScroll: 5,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            initialSlide: 2,
            padding: 40,
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            padding: 290,
            dots: false,
          }
        }
      ]
    };
    return (
      <div className="slid">
        <br />
        <div className='S__text'>
            <h2>Качество продукции подтверждают <span>сертификаты</span></h2>
          </div>
        <Slider {...settings}>
          <div className="c">
            <img src={ras1} alt="" />
          </div>
          <div className="c">
            <img src={ras2} alt="" />
          </div>
          <div className="c">
            <img src={ras3} alt="" />
          </div>
          <div className="c">
            <img src={ras4} alt="" />
          </div>
          <div className="c">
            <img src={ras5} alt="" />
          </div>

          <div className="c">
            <img src={ras2} alt="" />
          </div>
          <div className="c">
            <img src={ras1} alt="" />
          </div>
          <div className="c">
            <img src={ras4} alt="" />
          </div>
          <div className="c">
            <img src={ras5} alt="" />
          </div>
          <div className="c">
            <img src={ras3} alt="" />
          </div>

          <div className="c">
            <img src={ras2} alt="" />
          </div>
          <div className="c">
            <img src={ras3} alt="" />
          </div>
          <div className="c">
            <img src={ras1} alt="" />
          </div>
          <div className="c">
            <img src={ras4} alt="" />
          </div>
          <div className="c">
            <img src={ras5} alt="" />
          </div>
        </Slider>
      </div>
    );
  }
}