import React, { Component } from 'react'
import "./home.scss"

import iconka1 from "../../assets/images/n__png.png"
import iconka2 from "../../assets/images/n__fb.png"
import iconka3 from "../../assets/images/n__linkedin.png"
import btntagi from '../../assets/images/controls.png'


class Hom extends Component {
    render() {
        return <section>
            <div className='n__bg'>
                <div className="container prosta">
                    <div className='lft'> 
                        <p>LEANGROUP - тубы и этикетки</p><br />
                        <h1>Ламинатные тубы</h1><br />
                        <p>Используются для производства зубных паст. Широко применяются в сегменте косметики, пищевой индустрии, парафармацевтике, бытовой химии и DIY (Do-it-Yourself).</p>
                        <br />
                        <button>Каталог</button>
                    </div>
                    <div className='iconcha'>
                        <img src={iconka1} alt="" />
                        <img src={iconka2} alt="" />
                        <img src={iconka3} alt="" />
                    </div>
                </div>
                <br /><br /><br /><br /><br /><br /><br />
                <div className='container'>
                    <img src={btntagi} alt="" />
                </div>
                <br /><br />
            </div>
        </section>;
    }
}

export default Hom;
