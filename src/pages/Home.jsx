import React from 'react';
import Leangroup from '../components/leangroup/Leangroup';
import Novosti from '../components/novosti/Novosti';
import Footer from '../components/footer/Footer';
import SimpleSlider from '../components/sertifikat/Sertifikat';

import Nasha from '../components/nasha';
import NashaKomanda from '../components/nasha-komanda/Nasha-Komand';
import Informatsiyu from '../components/informatsiyu/informatsiyu';
import Header from '../components/header/Header';
import Hom from '../components/hom/Hom';






const Home = () => {
  return (
      <div>
        <Header />
        <main>
          <Hom/>
          <Leangroup />
          <SimpleSlider />
          <Nasha />
          <Informatsiyu/>
          <NashaKomanda />
          <Novosti />
        </main>
        <Footer />
      </div>
  );
};


export default Home;